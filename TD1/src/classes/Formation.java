package classes;

public class Formation {
	
	private String intitule;
	private int nbrJours;
	private Stagiaire[] Stagiaires;
	
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public int getNbrJours() {
		return nbrJours;
	}
	public void setNbrJours(int nbrJours) {
		this.nbrJours = nbrJours;
	}
	public Stagiaire[] getStagiaires() {
		return Stagiaires;
	}
	public void setStagiaires(Stagiaire[] stagiaires) {
		Stagiaires = stagiaires;
	}
	public Formation(String intitule, int nbrJours, Stagiaire[] stagiaires) {
		super();
		this.intitule = intitule;
		this.nbrJours = nbrJours;
		Stagiaires = stagiaires;
	}
	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public float calculerMoyenneFormation()
	{   
		float Somme=0;
		for (int i=0; i< this.Stagiaires.length; i++)
		{
			Somme=Somme+this.Stagiaires[i].calculerMoyenne();
		}
		return Somme/this.Stagiaires.length;
	}
	
	public int getIndexMax() {
		int i;
	 
	float m_max=this.Stagiaires[0].calculerMoyenne();
	int index=0;
	for (i=1;i<this.Stagiaires.length;i++)
	{ 
		if (this.Stagiaires[i].calculerMoyenne()> m_max)
		{
		m_max=this.Stagiaires[i].calculerMoyenne();
		index=i;
		}
	}
	
	return index;
	
	}
	
	public String afficherNomMax()
	
	{  
		return Stagiaires[this.getIndexMax()].getNom();
		
	}
	
	public int getIndexMin() {
		int i;
	 
	float m_min=this.Stagiaires[0].calculerMoyenne();
	int index=0;
	for (i=1;i<this.Stagiaires.length;i++)
	{ 
		if (this.Stagiaires[i].calculerMoyenne()< m_min)
		{
		m_min=this.Stagiaires[i].calculerMoyenne();
		index=i;
		}
	}
	
	return index;
	
	}
	
	public String afficherNomMin()
	
	{  
		return Stagiaires[this.getIndexMin()].getNom();
		
	}
	
	public float afficherMinMax()
	
	{
		return Stagiaires[this.getIndexMax()].trouvermin();
	}
	
	public float trouverMoyenneParNom(String N)
	
	{ 
		int i=0;
		Boolean trouve=false;
		
		while ((i< this.Stagiaires.length) && (!trouve))
		
	{
		if (this.Stagiaires[i].comparer(N))
			trouve=true;
		else 
			i++;
		
	}
		  if (trouve)
			return (this.Stagiaires[i].calculerMoyenne());
		  else 
			  
			  return  -1;
		
	}
	
	}
	
	
	
	


