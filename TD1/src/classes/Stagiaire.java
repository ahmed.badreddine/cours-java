package classes;

import java.util.Arrays;

public class Stagiaire {
	
	private String nom;
	private float [] notes;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public float[] getNotes() {
		return notes;
	}
	public void setNotes(float[] notes) {
		this.notes = notes;
	}
	
	public Stagiaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Stagiaire(String nom, float[] notes) {
		super();
		this.nom = nom;
		this.notes = notes;
	}
	
	public float calculerMoyenne()
	{
		float Somme =0;
		
		for (int i=0; i<this.notes.length; i++)
		{
			Somme=Somme + this.notes[i];
		}
		return Somme/this.notes.length;
		
	}
	

	public float trouvermax()
	{
		
		float max= this.notes[0];
		
		
		for (int i=1; i<this.notes.length; i++)
		{
		  if (this.notes[i]	> max)
		  {
			  max=this.notes[i];
		  }
		}
		
		
		return max;
		
	}
	
	
	public float trouvermin()
	{
		
		float min= this.notes[0];
		
		
		for (int i=1; i<this.notes.length; i++)
		{
		  if (this.notes[i]	< min)
		  {
			  min=this.notes[i];
		  }
		}
		
		return min;
		
	}
	
	@Override
	public String toString() {
		return  nom + ", la liste des  notes=" + Arrays.toString(notes) + "";
	}
	
	public Boolean comparer(Stagiaire S)
	{
		if (this.calculerMoyenne()> S.calculerMoyenne())
			return true;
		
		else return false;
	}
	
	public Boolean comparer (String Nom)
	{  
	
	if (this.nom.equals(Nom))
		return true ;
	else return false;
				
				
	
	}

	}
	
	

